const config = {
  rabbit: {
    host: process.env.RABBIT_HOST || 'localhost',
    user: process.env.RABBIT_USER || 'rabbitUser',
    password: process.env.RABBIT_PASSWORD || 'rabbitUser',
    queue: {
      test: process.env.RABBIT_QUEUE_EMAIL || 'test_queue',
    },
  },
  postgres: {
    user: process.env.DB_USER || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    database: process.env.DB_NAME || 'test_db',
    password: process.env.DB_PASSWORD || 'example',
    port: process.env.DB_PORT || 5432,
  },
};

module.exports = config;
