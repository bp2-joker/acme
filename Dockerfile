FROM mhart/alpine-node:8

RUN apk add --no-cache bash

RUN mkdir /project
ADD package.json /project
ADD yarn.lock /project

WORKDIR /project

RUN yarn --production

COPY . .

RUN chmod +x ./wait-for-it.sh 

# CMD ["yarn","start"]

# EXPOSE 3030
