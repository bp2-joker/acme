-- auto-generated definition
create schema dbo;
create table dbo.files
(
    id      serial                not null
        constraint files_pk
            primary key,
    name    varchar(1000)         not null,
    isready boolean default false not null,
    iserror boolean default false not null,
    isworking boolean NOT NULL DEFAULT false,
    startworking timestamp without time zone
);

alter table dbo.files
    owner to postgres;

create unique index files_id_uindex
    on dbo.files (id);

