const fs = require("fs");
const zlib = require("zlib");
const postgres = require("./connection/postgres");
const config = require("./config");

const inFolder = "./in/";
const outFolder = "./out/";

if (!fs.existsSync(outFolder)) {
  fs.mkdirSync(outFolder);
}

let postgresClient = null;

async function zipFile(info) {
  const change = () =>
    new Promise((resolve, reject) => {
      fs.createReadStream(`${inFolder}${info.name}`)
        .on("error", reject)
        .pipe(zlib.createGzip())
        .on("error", reject)
        .pipe(fs.createWriteStream(`${outFolder}${info.name}.gz`))
        .on("error", reject)
        .on("finish", resolve);
    });
  let isReady = false;
  let isError = false;
  try {
    await change();
    isReady = true;
    console.log(`Обработка. ${info.id} - Успешно`);
  } catch (err) {
    isError = true;
    console.log(`Обработка. ${info.id} - Ошибка`);
  }

  return { isReady, isError };
}

async function main() {
  postgresClient = await postgres(config.postgres);
  // await postgresClient.fileDeleteAll();
  const scan = () =>
    new Promise((resolve, reject) => {
      fs.readdir(inFolder, async (err, files) => {
        if (err) reject(err);
        const ids = await postgresClient.fileInsert(files);
        resolve(ids);
      });
    });
  try {
    await scan();
    console.log("Директория просканирована. Данные записаны в базу");
  } catch (err) {
    console.log(err);
    console.log("Ошибка сканирования директории");
  }

  //получаем файл
  let info = null;
  while (await postgresClient.getFileWorking(data => (info = data))) {
    const file = info[0];
    let timerWorking = setInterval(async () => {
      await postgresClient.fileUpdateWorkingTime(file.id);
    }, 150 * 1000);
    const resZip = await zipFile(file);
    clearInterval(timerWorking);
    await postgresClient.fileUpdate(file.id, resZip.isReady, resZip.isError);
  }

  console.log("Все файлы обработаны");
  await postgresClient.close();
}

main();
