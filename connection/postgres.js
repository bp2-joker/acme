const Pool = require("pg").Pool;

async function postgresClient(config) {
  const pool = new Pool(config);

  async function fileDeleteAll() {
    await pool.query("TRUNCATE dbo.files");
  }

  async function fileInsert(names) {
    const res = await pool.query(
      `INSERT INTO dbo.files(name)
      SELECT t.name
      FROM (SELECT unnest(array[$1::varchar(1000)[]]) as name) t
          LEFT OUTER JOIN dbo.files f on f.name=t.name
      WHERE f.id IS NULL
      RETURNING id;`,
      [names]
    );
    return res.rows;
  }
  async function fileRead(id) {
    const res = await pool.query("SELECT * FROM dbo.files WHERE id=$1", [id]);
    return res.rows;
  }
  async function fileUpdate(id, isReady, isError) {
    await pool.query(
      "UPDATE dbo.files SET isReady=$2, isError=$3, isworking=false WHERE id=$1",
      [id, isReady, isError]
    );
  }

  async function fileUpdateWorkingTime(id) {
    await pool.query("UPDATE dbo.files SET startworking=now() WHERE id=$1", [
      id
    ]);
  }

  async function getFileWorking(setInfo) {
    const res = await pool.query(
      `UPDATE dbo.files
    SET isworking=true, startworking=now()
    WHERE id in (SELECT id FROM dbo.files 
          WHERE (isworking=false OR EXTRACT(MINUTE FROM now()-startworking)>3)
               and isready=false and iserror=false limit 1)
    RETURNING id, name;`
    );
    setInfo(res.rows);
    return res.rows.length !== 0;
  }

  async function close() {
    await pool.end();
  }

  return {
    close,
    fileInsert,
    fileRead,
    fileUpdate,
    fileDeleteAll,
    getFileWorking,
    fileUpdateWorkingTime
  };
}

module.exports = postgresClient;
